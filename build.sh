#!/bin/bash
set -xe
shopt -s extglob

BUILD_DIR=workdir

# From https://stackoverflow.com/a/48808214
args=("$@")
for ((i=0; i<"${#args[@]}"; ++i)); do
    case ${args[i]} in
        -b) BUILD_DIR=${args[i+1]}; unset args[i]; unset args[i+1]; break;;
    esac
done

[ -d build ] || git clone https://gitlab.com/ubports/porting/community-ports/halium-generic-adaptation-build-tools.git -b main build

HERE=$(pwd)
SCRIPT="$(dirname "$(realpath "$0")")"/build
if [ ! -d "$SCRIPT" ]; then
    SCRIPT="$(dirname "$SCRIPT")"
fi
TMPDOWN="$BUILD_DIR/downloads"
mkdir -p "$TMPDOWN"

source deviceinfo
source "$SCRIPT/common_functions.sh"
source "$SCRIPT/setup_repositories.sh" "${TMPDOWN}"

KERNEL_DIR="$(basename "${deviceinfo_kernel_source}")"
KERNEL_DIR="${KERNEL_DIR%.*}"
echo $KERNEL_DIR

cd "$TMPDOWN/$KERNEL_DIR"

# # Generate all-yes configs
# # ./scripts/gki/fragment_allyesconfig.sh arch/arm64/configs/vendor/lahaina-qgki_defconfig arch/arm64/configs/vendor/lahaina-qgki-allyes_defconfig
# # ./scripts/gki/fragment_allyesconfig.sh arch/arm64/configs/vendor/xiaomi_QGKI.config arch/arm64/configs/vendor/xiaomi_QGKI-allyes.config
# # ./scripts/gki/fragment_allyesconfig.sh arch/arm64/configs/vendor/lisa_QGKI.config arch/arm64/configs/vendor/lisa_QGKI-allyes.config

cd "$HERE"

./build/build.sh "${args[@]}" -b "$BUILD_DIR"